@extends('base')



@section('main')
<h4>Add Organizer</h4>


<form action="{{ route('organizer.store') }}" method="post">
    {{csrf_field()}}
    <div class="form-group">
        <label for="title" class="control-label">Title</label>
        <input type="text" class="form-control" name="organizerName" placeholder="Title">
    </div>
    <div class="form-group">
        <label for="content" class="control-label">imageLocation</label>
        <textarea name="imageLocation" cols="30" rows="5" class="form-control"></textarea>
    </div>
    <div class="form-group">
        <button type="submit" class="btn btn-info">Simpan</button>        
    </div>
</form>
@endsection