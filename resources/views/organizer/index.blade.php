@extends('base')



@section('main')



<table class="table table-responsive martop-sm">
  <thead>
      <th>ID</th>
      <th>Title</th>
      <th>Image</th>
  </thead>
  <tbody>
  @foreach($organizer as $p)
  <tr>
    <td>{{ $p->id }}</td>
    <td>{{ $p->organizerName }}</td>
    <td>{{ $p->imageLocation }}</td>
  </tr>
  @endforeach
  </tbody>
</table>
  
  
@endsection