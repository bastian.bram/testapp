<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//list
Route::get('/', 'TodoController@index');
//form create
Route::get('/todos/create','TodoController@create');
// add
Route::post('/todos','TodoController@store');
//edit
Route::get('/todos/{todo}/edit', 'TodoController@edit');
//update
Route::put('/todos/{todo}', 'TodoController@update');
//delete
Route::get('/todos/{todo}/delete', 'TodoController@delete');
Route::delete('delete-multiple-category', ['as'=>'todos.multiple-delete','uses'=>'TodoController@deleteMultiple']);