<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::apiResource('organizer','OrganizerController',[
    'only' => ['index','create','show','store'],
]);
Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


// Route::get('organizer', 'OrganizerController@index');
// Route::get('organizer/{id}', 'OrganizerController@show');
// Route::post('organizer', 'OrganizerController@store');
// Route::put('organizer/{id}', 'OrganizerController@update');
// Route::delete('organizer/{id}', 'OrganizerController@destroy');